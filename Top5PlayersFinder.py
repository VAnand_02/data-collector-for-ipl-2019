import pandas as pd


class Top5PlayersFinder:

    @staticmethod
    def get_top5_run_scoring_batsman(team1, team2):
        df = pd.read_csv('iplbatting2019.csv')
        find_scorer = df[['team', 'Batsman', 'R']]
        dataset = (find_scorer.groupby(['team', 'Batsman']).sum()).sort_values(by=['team', 'R'], ascending=False)

        # print('****** IPL Top scoring Batsman*****')
        # print(dataset)
        # print('\n\n\n\n\n')

        team_players = dict()
        for team_Batsman, Run in dataset.iteritems():
            for team, run in Run.iteritems():
                if team[0] in team_players:
                    team_players[team[0]].append((team[1], run))
                else:
                    team_players[team[0]] = list()
                    team_players[team[0]].append((team[1], run))

        filtered_team_players = {k: v for (k, v) in team_players.items() if (k == team1 or k == team2)}
        for team, players in filtered_team_players.items():
            print("****** {}'s Top 8 batsman*****".format(team))
            for player, run in sorted(players, key=lambda x: x[1], reverse=True)[:8]:
                print(team, player, run)
            print('\n\n')
        print('\n\n')

    @staticmethod
    def get_top5_wicket_taking_bowler(team1, team2):
        df = pd.read_csv('iplbowling2019.csv')
        find_scorer = df[['team', 'Bowler', 'W']]
        dataset = (find_scorer.groupby(['team', 'Bowler']).sum()).sort_values(by=['team', 'W'], ascending=False)

        # print('****** IPL Top wicket taking Bowler*****')
        # print(dataset)
        # print('\n\n\n\n\n')

        team_players = dict()
        for team_Batsman, Run in dataset.iteritems():
            for team, run in Run.iteritems():
                if team[0] in team_players:
                    team_players[team[0]].append((team[1], run))
                else:
                    team_players[team[0]] = list()
                    team_players[team[0]].append((team[1], run))

        filtered_team_players = {k: v for (k, v) in team_players.items() if (k == team1 or k == team2)}
        for team, players in filtered_team_players.items():
            print("****** {}'s Top 8 bowler*****".format(team))
            for player, run in sorted(players, key=lambda x: x[1], reverse=True)[:8]:
                print(team, player, run)
            print('\n\n')






