
# !/sh/bin/env python

from Top5PlayersFinder import Top5PlayersFinder
from HtmlParser_CricBuzz import HtmlParser
from IPLDataFetcherAndLogger import IPLDataFetcherAndLogger


def isvalid_team_name(team_name):
    all_teams = HtmlParser.team_name_short_forms.values()
    return False if team_name not in all_teams else True


def main():
    is_new_data_available = IPLDataFetcherAndLogger.fetch_and_log(True, True, True)
    if not is_new_data_available:
        print('New data not available, will show players information based on old data, if found.')

    while 1:

        team1 = input('Enter first team name = ')
        if isvalid_team_name(team1):
            team2 = input('Enter second team name = ')
            if isvalid_team_name(team2):
                if team1 != team2:
                    Top5PlayersFinder.get_top5_run_scoring_batsman(team1, team2)
                    Top5PlayersFinder.get_top5_wicket_taking_bowler(team1, team2)
                else:
                    print("Both team name can't be same...")
            else:
                print('Invalid team name!!!')
        else:
            print('Invalid team name!!!')

        wish_to_continue = input('Do you continue(Y/N) ?')
        if wish_to_continue == 'N' or wish_to_continue == 'n':
            break


if __name__ == '__main__':
    main()
















