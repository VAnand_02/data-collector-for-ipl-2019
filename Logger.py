
import os


class Logger:

    @staticmethod
    def log_batting_data(batting_data_list, write_completion_indicator):

        Logger.write('iplbatting2019.csv', batting_data_list)
        write_completion_indicator()

    @staticmethod
    def log_bowling_data(bowling_data_list, write_completion_indicator):

        Logger.write('iplbowling2019.csv', bowling_data_list)
        write_completion_indicator()

    @staticmethod
    def log_match_result(match_result_list, write_completion_indicator):

        Logger.write('iplresult2019.csv', match_result_list)
        write_completion_indicator()

    @staticmethod
    def get_match_id_for_logged_matches():

        current_path = os.getcwd()
        file_path = os.path.join(current_path, 'iplresult2019.csv')
        with open(file_path, 'r') as file_handler:

            if os.path.getsize(file_path) > 0:
                match_ids = []
                for line in file_handler.readlines()[1:]:
                    match_ids.append(line.split(',')[0])
                return match_ids
            else:
                return None

    @staticmethod
    def write(file_name, data):

        current_path = os.getcwd()
        file_path = os.path.join(current_path, file_name)
        with open(file_path, 'a+') as file_handler:

            if os.path.getsize(file_path) > 0:
                for d in data[1:]:
                    file_handler.write(d+'\n')
            else:
                for d in data:
                    file_handler.write(d+'\n')
