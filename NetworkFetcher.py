import requests
import Reachability


class NetworkFetcher:

    @staticmethod
    def fetch(url):
        if Reachability.is_connected():
            return requests.get(url)
        else:
            return None


