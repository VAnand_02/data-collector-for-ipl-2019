
import socket


class Reachability:

    @staticmethod
    def is_connected():
        host_name = 'www.google.com'
        try:
            host = socket.gethostbyname(host_name)
            socket.create_connection((host, 80))
            return True
        except OSError:
            pass

        return False


