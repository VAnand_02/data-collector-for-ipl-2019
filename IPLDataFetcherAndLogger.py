
from NetworkFetcher import NetworkFetcher
from HtmlParser_CricBuzz import HtmlParser
from Logger import Logger
import re


class IPLDataFetcherAndLogger:

    # url_list = ['https://www.cricbuzz.com/live-cricket-scorecard/22396/csk-vs-rcb-1st-match-indian-premier-league-2019'

    @staticmethod
    def log_completion_indicator(team1, team2, record_type):
        print('Logging completed for {} vs {} {}'.format(team1, team2, record_type))

    @staticmethod
    def fetch_and_log(batting_logger_flag, bowling_logger_flag, match_result_logger_flag):

        response = NetworkFetcher.fetch('https://www.cricbuzz.com/cricket-series/2810/indian-premier-league-2019/matches')
        if response is None:
            print("'Can't download latest data, check your internet connection...")
            return False

        match_urls = HtmlParser.get_ipl2019_league_stage_calendar(response)
        logged_match_ids = Logger.get_match_id_for_logged_matches()
        if batting_logger_flag or bowling_logger_flag or match_result_logger_flag:

            for url in match_urls:
                match_id = re.search(r'\d{5,}', url)
                if logged_match_ids is not None and match_id is not None:
                    if match_id.group(0) in logged_match_ids:
                        print("{}'s data already logged, moving on to next the url...".format(match_id.group(0)))
                        continue

                # print(url)
                response = NetworkFetcher.fetch(url)
                HtmlParser.parse_score_card_url_response(response)

                inning1_batting_team_name = HtmlParser.get_inning1_batting_team_name()
                inning1_bowling_team_name = HtmlParser.get_inning1_bowling_team_name()
                inning2_batting_team_name = HtmlParser.get_inning2_batting_team_name()
                inning2_bowling_team_name = HtmlParser.get_inning2_bowling_team_name()

                batting_information_inning1 = HtmlParser.innings_information['inning1_batting_information']
                batting_information_inning2 = HtmlParser.innings_information['inning2_batting_information']
                bowling_information_inning1 = HtmlParser.innings_information['inning1_bowling_information']
                bowling_information_inning2 = HtmlParser.innings_information['inning2_bowling_information']

                if batting_logger_flag:
                    # Batting
                    batting_card = HtmlParser.get_batting_card(batting_information_inning1, inning1_batting_team_name, inning1_bowling_team_name)
                    Logger.log_batting_data(batting_card, lambda: IPLDataFetcherAndLogger.log_completion_indicator(inning1_batting_team_name, inning1_bowling_team_name, 'Batting'))
                    batting_card = HtmlParser.get_batting_card(batting_information_inning2, inning2_batting_team_name, inning2_bowling_team_name)
                    Logger.log_batting_data(batting_card, lambda: IPLDataFetcherAndLogger.log_completion_indicator(inning1_batting_team_name, inning1_bowling_team_name, 'Batting'))

                if bowling_logger_flag:
                    # Bowling
                    bowling_card = HtmlParser.get_bowling_card(bowling_information_inning1, inning1_batting_team_name, inning1_bowling_team_name)
                    Logger.log_bowling_data(bowling_card, lambda: IPLDataFetcherAndLogger.log_completion_indicator(inning1_batting_team_name, inning1_bowling_team_name, 'Bowling'))
                    bowling_card = HtmlParser.get_bowling_card(bowling_information_inning2, inning2_batting_team_name, inning2_bowling_team_name)
                    Logger.log_bowling_data(bowling_card, lambda: IPLDataFetcherAndLogger.log_completion_indicator(inning1_batting_team_name, inning1_bowling_team_name, 'Bowling'))

                if match_result_logger_flag:
                    # Result
                    match_result = HtmlParser.get_match_information(response, match_id.group(0), inning1_batting_team_name, inning1_bowling_team_name)
                    Logger.log_match_result(match_result, lambda: IPLDataFetcherAndLogger.log_completion_indicator(inning1_batting_team_name, inning1_bowling_team_name, ''))
        return True
