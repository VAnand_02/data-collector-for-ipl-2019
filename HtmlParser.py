

import re


class HtmlParser:

    columns_batting = ['Batsman', '', 'R', 'B', '4s', '6s', 'SR']
    columns_batting_filter_flag = [True, False, True, False, False, False, False]

    columns_bowling = ['Bowler', 'O', 'M', 'R', 'W', 'NB', 'WD', 'ECO']
    columns_bowling_filter_flag = [True, False, False, False, True, False, False, False]

    team_names = []
    innings_information = {}
    team_name_short_forms = {'Chennai Super Kings': 'csk',
                             'Chennai': 'csk',
                             'Royal Challengers Bangalore': 'rcb',
                             'Bangalore': 'rcb',
                             'Kolkata Knight Riders': 'kkr',
                             'Kolkata': 'kkr',
                             'Sunrisers Hyderabad': 'srh',
                             'Hyderabad': 'srh',
                             'Rajasthan Royals': 'rr',
                             'Rajasthan': 'rr',
                             'Kings XI Punjab': 'kxip',
                             'Punjab': 'kxip',
                             'Delhi Capitals': 'dc',
                             'Delhi': 'dc',
                             'Mumbai Indians': 'mi',
                             'Mumbai': 'mi'}

    @staticmethod
    def parse(match_information):
        for inning_index, inning_information in enumerate(match_information.find_all('div', id=re.compile('^(innings_)1|2'))):

            batting_bowling_information = inning_information.find_all('div', class_='cb-col cb-col-100 cb-ltst-wgt-hdr')
            for batting_bowling_index, information in enumerate(batting_bowling_information[:2]):

                batting_bowling_key = 'batting' if batting_bowling_index == 0 else 'bowling'
                key = 'inning{}_{}_information'.format((inning_index+1), batting_bowling_key)
                HtmlParser.innings_information[key] = information
        return HtmlParser.innings_information

    @staticmethod
    def get_inning1_batting_team_name():

        inning_information = HtmlParser.innings_information['inning1_batting_information']
        return HtmlParser.get_team_name(inning_information)

    @staticmethod
    def get_inning1_bowling_team_name():

        inning_information = HtmlParser.innings_information['inning2_batting_information']
        return HtmlParser.get_team_name(inning_information)

    @staticmethod
    def get_inning2_batting_team_name():

        inning_information = HtmlParser.innings_information['inning2_batting_information']
        return HtmlParser.get_team_name(inning_information)

    @staticmethod
    def get_inning2_bowling_team_name():

        inning_information = HtmlParser.innings_information['inning1_batting_information']
        return HtmlParser.get_team_name(inning_information)

    @staticmethod
    def get_team_name(inning_information):

        inning_name = inning_information.find('div', class_='cb-col cb-col-100 cb-scrd-hdr-rw')
        name_fragments = inning_name.text.split()
        return ' '.join(name_fragments[0:len(name_fragments) - 3])

    @staticmethod
    def get_batting_card(batting_information, batting_team, bowling_team):

        batting_header_title = []
        batting_score_card = []

        batting_header = batting_information.find('div', class_='cb-col cb-col-100 cb-scrd-sub-hdr cb-bg-gray')
        for index, value in enumerate(batting_header.find_all('div')):
            if len(value.text) > 0 and HtmlParser.columns_batting_filter_flag[index]:
                batting_header_title.append(value.text)
        batting_header_title.append('team')
        batting_header_title.append('opponent_team')
        batting_score_card.append(','.join(batting_header_title))

        batting_scores = batting_information.find_all('div', class_='cb-col cb-col-100 cb-scrd-itms')
        for batting_score in batting_scores:
            title = batting_score.find('div').text
            if title == 'Extras' or title == 'Total' or title == 'Did not Bat':
                break
            player_record = []
            for index, value in enumerate(batting_score.find_all('div')):
                if len(value.text) > 0 and HtmlParser.columns_batting_filter_flag[index]:
                    player_record.append(value.text)
            player_record.append(HtmlParser.team_name_short_forms[batting_team])
            player_record.append(HtmlParser.team_name_short_forms[bowling_team])
            batting_score_card.append(','.join(player_record))

        return batting_score_card

    @staticmethod
    def get_bowling_card(bowling_information, bowling_team, batting_team):

        bowling_header_title = []
        bowling_score_card = []
        bowling_header = bowling_information.find('div', class_='cb-col cb-col-100 cb-scrd-sub-hdr cb-bg-gray')

        for index, value in enumerate(bowling_header.find_all('div')):
            if len(value.text) > 0 and HtmlParser.columns_bowling_filter_flag[index]:
                bowling_header_title.append(value.text)
        bowling_header_title.append('team')
        bowling_header_title.append('opponent_team')
        bowling_score_card.append(','.join(bowling_header_title))

        batting_scores = bowling_information.find_all('div', class_='cb-col cb-col-100 cb-scrd-itms')
        for batting_score in batting_scores:
            player_record = []
            for index, value in enumerate(batting_score.find_all('div')):
                if len(value.text) > 0 and HtmlParser.columns_bowling_filter_flag[index]:
                    player_record.append(value.text)
            player_record.append(HtmlParser.team_name_short_forms[batting_team])
            player_record.append(HtmlParser.team_name_short_forms[bowling_team])
            bowling_score_card.append(','.join(player_record))

        return bowling_score_card

    @staticmethod
    def get_match_information(match_information, team1, team2):

        result = []
        header_string = '{}, {}, {}, {}, {}'.format('team1', 'team2', 'city', 'winner', 'type')
        match_result = match_information.find('div', class_='cb-col cb-scrcrd-status cb-col-100 cb-text-complete')
        result_information = HtmlParser.get_match_result(match_result.text)
        venue_information = match_information.find('div', class_='cb-nav-subhdr cb-font-12')
        venue = venue_information.findChildren("a", recursive=False)[1]['title'].split(',')[1]
        result_string = '{}, {}, {}, {}, {}'.format(team1, team2, venue, result_information[0], result_information[1])
        result.append(header_string)
        result.append(result_string)
        return result

    @staticmethod
    def get_match_result(match_result):

        match = re.search(r'\((.*)\)', match_result)
        result_fragment = match.group(1).split('won') if match is not None else match_result.split('won')
        winner_team = result_fragment[0].strip()
        search_string = result_fragment[1].strip()
        win_type = 'score_chased' if re.search(r'by\s\d+\s(wkt)s?', search_string) is not None else 'score_set' if re.search(r'by\s\d+\s(run)s?', search_string) else search_string.split()[1]
        print(win_type)
        return HtmlParser.team_name_short_forms[winner_team], win_type

    @staticmethod
    def get_ipl2019_league_stage_calendar(calendar_information):
        match_urls = []
        all_matches = calendar_information.find_all('div', class_='cb-col-60 cb-col cb-srs-mtchs-tm')
        for match_information in all_matches:
            if HtmlParser.is_match_completed(match_information):
                match_url = HtmlParser.get_url((match_information.a['href'], match_information.find('span').text.split(',')[0]))
                match_urls.append(match_url)
        return match_urls

    @staticmethod
    def get_url(match_information):
        match_url = match_information[0]
        string_to_replace = 'live-cricket-scores' if 'live-cricket-scores' in match_url else 'cricket-scores'
        match_url = match_url.replace(string_to_replace, 'live-cricket-scorecard', 1)
        playing_teams = match_information[1].split('vs')
        team1 = HtmlParser.team_name_short_forms[playing_teams[0].strip()]
        team2 = HtmlParser.team_name_short_forms[playing_teams[1].strip()]
        teams = '{}-vs-{}'.format(team1, team2)
        match_url = match_url.replace('vs', teams, 1)
        full_url = 'https://www.cricbuzz.com' + match_url
        return full_url

    @staticmethod
    def is_match_completed(match_information):
        result = match_information.find('a', class_='cb-text-complete')
        return True if result is not None else False


